#
# This Makefile requires GNU make.
#
# Do not make changes here.
# Use the included .mak files.
#

make_need := 3.81
ifeq "" "$(strip $(filter $(make_need), $(firstword $(sort $(make_need) $(MAKE_VERSION)))))"
fail := $(error Your make ($(MAKE_VERSION)) is too old. You need $(make_need) or newer)
endif

-include config.mak
include package/targets.mak

INSTALL := ./tools/install.sh

install: install-module install-script install-seed

install-module: $(MODULE_TARGET:module/%=$(DESTDIR)$(service_directory)/$(package)/%)
install-script: $(SCRIPT_TARGET:module/configure/%=$(DESTDIR)$(service_directory)/$(package)/configure/%)
install-seed: $(SEED_TARGET:module/configure/session=$(DESTDIR)$(seed_directory)/session)


$(DESTDIR)$(service_directory)/$(package)/%: module/%
	exec $(INSTALL) -D -m 644 $< $@
	sed -i -e 's,@VERSION@,$(version),' \
		-e "s,@DISPLAY_MANAGER@,$(DISPLAY_MANAGER)," \
		-e "s,@CONSOLE_TRACKER@,$(CONSOLE_TRACKER)," \
		-e "s,@XDG_RUNTIME@,$(XDG_RUNTIME)," \
		-e "s,@DESKTOP_CMDLINE@,$(DESKTOP_CMDLINE)," $@

$(DESTDIR)$(service_directory)/$(package)/configure/%: module/configure/%
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(seed_directory)/session: module/configure/session
	exec $(INSTALL) -D -m 644 $< $@

version:
	@echo $(version)

.PHONY: install version

.DELETE_ON_ERROR:
